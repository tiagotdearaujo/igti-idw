import Vue from 'vue/dist/vue.min.js'
import sayHello from './say_hello'

/* eslint-disable no-unused-vars */
const app = new Vue({
  el: '#app',
  data: {
    title: 'Aula de IDW',
    message: sayHello('Vue')
  }
})
/* eslint-enable no-unused-vars */
