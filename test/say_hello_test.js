/* eslint-env mocha */

import { strictEqual } from 'assert'
import sayHello from '../src/say_hello'

describe('sayHello test', () => {
  it('retorna um oi', () => {
    strictEqual(sayHello('test'), 'Hello test!')
  })
})
